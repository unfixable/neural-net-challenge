import numpy as np


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


def sigmoid_deriv(x):
    return x * (1 - x)


if __name__ == "__main__":

    # input
    X = np.array([[0, 0, 1],
                  [0, 1, 1],
                  [1, 0, 1],
                  [1, 1, 1]])
    # labels
    Y = np.array([[0],
                  [0],
                  [1],
                  [0]])

    W = 2 * np.random.random((3, 1)) - 1
    print("Starting weights: " + str(W))

    for i in range(100000):
        Y_pred = sigmoid(np.dot(X, W))
        error = Y - Y_pred
        print("Error:" + str(np.mean(np.abs(error))))
        adjustment = np.dot(Y.T, error * sigmoid_deriv(Y_pred))
        W += adjustment

    print("New weights: " + str(W))
    
    X_test = np.array([1, 0, 0])
    Y_pred_test = sigmoid(np.dot(X_test, W))
    print("Test input:" + str(X_test))
    print("Prediction:" + str(Y_pred_test))
